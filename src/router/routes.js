const routes = [
  {
    path: "/",
    component: () => import("pages/MoviesCollection.vue"),
  },

  {
    path: "/add",
    component: () => import("pages/UpdateMovie.vue"),
  },

  {
    path: "/:id",
    component: () => import("pages/UpdateMovie.vue"),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
