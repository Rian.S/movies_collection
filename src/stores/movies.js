import { defineStore } from "pinia";
import movieData from "../data/movies.json";

export const useMovieStore = defineStore("movie", {
  state: () => ({
    movies: movieData,
    movie: {},
    loading: false,
  }),
  getters: {
    getMovies: (state) => state.movies,
    getMovie: (state) => state.movie,
    getLoading: (state) => state.loading,
  },
  actions: {
    fetchMovies() {
      return new Promise((resolve, reject) => {
        try {
          // change the method to get the data accordingly, like using RestAPI
          // const movies = movieData;
          // this.movies = movies;
          resolve();
        } catch (err) {
          console.error("Error: ", err);
          reject();
        }
      });
    },
    fetchMovieById(id) {
      return new Promise((resolve, reject) => {
        try {
          this.fetchMovies()
            .then(() => {
              const movie = this.movies.filter(
                (movie) => movie.id === Number(id)
              );

              if (movie) {
                this.movie = movie;
              } else {
                this.movie = {};
              }
              resolve(movie);
            })
            .catch((err) => {
              console.error('Error: ", err');
              reject();
            });
        } catch (err) {
          console.error("Error: ", err);
          reject();
        }
      });
    },
    addMovie(movieData) {
      return new Promise((resolve, reject) => {
        try {
          const lastMovie = this.movies[this.movies.length - 1];
          movieData.id = lastMovie.id + 1;
          this.movies.push(movieData);
          resolve(movieData);
        } catch (err) {
          console.error("Error: ", err);
          reject();
        }
      });
    },
    updateMovie(movieData) {
      return new Promise((resolve, reject) => {
        try {
          const movie = this.movies.filter(
            (movie) => movie.id === parseInt(movieData.id)
          );

          if (movie) {
            const filterMovies = this.movies.filter(
              (movie) => movie.id !== parseInt(movieData.id)
            );
            // this.movies = [...filterMovies, movieData];
            const unsortedArray = [...filterMovies, movieData];
            this.movies = unsortedArray.sort((a, b) =>
              a.id > b.id ? 1 : a.id < b.id ? -1 : 0
            );

            resolve(movieData);
          } else {
            console.log("no movie");
            resolve();
          }
        } catch (err) {
          console.error("Error: ", err);
          reject();
        }
      });
    },
    deleteMovie(movieData) {
      return new Promise((resolve, reject) => {
        try {
          const filterMovies = this.movies.filter(
            (movie) => movie.id !== parseInt(movieData.id)
          );
          this.movies = filterMovies;

          resolve(movieData);
        } catch (err) {
          console.error("Error: ", err);
          reject();
        }
      });
    },
  },
});
