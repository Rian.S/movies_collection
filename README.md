# movies_collection (movies_collection)

CRUD responsive webapp for managing movies collection

Feature:

- responsive.
- searchable by title (using debounce)
- list movies collection, add movie, update, and delete
- form validation on add and update
- build using Vue 3 and Quasar
- in-memory data

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
